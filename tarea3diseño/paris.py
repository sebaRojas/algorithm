import urllib2  # descargar una url
import re  #  expresiones regulares
import string

def toint(precio):
    digitos = re.findall(r'([0-9]+)', precio)
    entero = int(''.join(digitos))
    return entero

def talla38():
    url = 'http://www.paris.cl/webapp/wcs/stores/servlet/SearchDisplay?searchTermScope=&searchType=1000&filterTerm=&maxPrice=&showResultsPage=true&langId=-5&beginIndex=0&sType=SimpleSearch&metaData=YWRzX2YyNTUwMV9udGtfY3M6IlJ1bm5pbmciPE1UQFNQPmFkc19mMjc1MDFfbnRrX2NzOiIzOCI%3D&pageSize=30&manufacturer=&resultCatEntryType=&catalogId=40000000629&pageView=image&searchTerm=&minPrice=&categoryId=51206332&storeId=10801&orderBy=4'
    response = urllib2.urlopen(url)
    html = response.read()



    #detalle
    detalle = re.findall(r'<div class="description_fixedwidth"(.+?)</div>', html, flags = re.DOTALL)
    _detalle_=[]
    for i in range(len(detalle)):
        detalle_ = re.findall(r'href(.+?)a>', detalle[i] , flags = re.DOTALL)
        detalle_ = re.findall(r'Zapatilla(.+?)\r', detalle_[0] , flags = re.DOTALL)
        _detalle_.append(detalle_)

    #marca
    marca=[]
    for i in _detalle_:
        marca.append(re.findall(r' [A-Za-z]* ', i[0], flags = re.DOTALL))

    #precio
    precio = re.findall(r'<div id="offerPrice" class="price offerPrice bold">(.+?)</div>', html ,flags = re.DOTALL)
    precio_=[]
    for i in precio:
        precio_.append(re.findall(r'\w+.\w+',i,flags = re.DOTALL))

    min_=precio_.index(min(precio_))

    datos=[_detalle_[min_][0],toint(precio_[min_][0])]
    return datos

def talla40():
    url = 'http://www.paris.cl/webapp/wcs/stores/servlet/SearchDisplay?searchTermScope=&searchType=1000&filterTerm=&orderBy=4&maxPrice=&showResultsPage=true&langId=-5&beginIndex=0&sType=SimpleSearch&metaData=YWRzX2YyNTUwMV9udGtfY3M6IlJ1bm5pbmci&pageSize=30&manufacturer=&resultCatEntryType=&catalogId=40000000629&pageView=image&searchTerm=&facet=ads_f104501_ntk_cs%253A%2522CL%2B40%2B%257C%2BUS%2B8.5%2B%257C%2BUK%2B8%2B%257C%2BEU%2B41%2522&minPrice=&categoryId=51206332&storeId=10801'
    response = urllib2.urlopen(url)
    html = response.read()



    #detalle
    detalle = re.findall(r'<div class="description_fixedwidth"(.+?)</div>', html, flags = re.DOTALL)
    _detalle_=[]
    for i in range(len(detalle)):
        detalle_ = re.findall(r'href(.+?)a>', detalle[i] , flags = re.DOTALL)
        detalle_ = re.findall(r'Zapatilla(.+?)\r', detalle_[0] , flags = re.DOTALL)
        _detalle_.append(detalle_)

    #marca
    marca=[]
    for i in _detalle_:
        marca.append(re.findall(r' [A-Za-z]* ', i[0], flags = re.DOTALL))

    #precio
    precio = re.findall(r'<div id="offerPrice" class="price offerPrice bold">(.+?)</div>', html ,flags = re.DOTALL)
    precio_=[]
    for i in precio:
        precio_.append(re.findall(r'\w+.\w+',i,flags = re.DOTALL))

    min_=precio_.index(min(precio_))

    datos=[_detalle_[min_][0],toint(precio_[min_][0])]
    return datos

def talla42():
    url = 'http://www.paris.cl/webapp/wcs/stores/servlet/SearchDisplay?searchTermScope=&searchType=1000&filterTerm=&orderBy=2&maxPrice=&showResultsPage=true&langId=-5&beginIndex=0&sType=SimpleSearch&metaData=YWRzX2YyNTUwMV9udGtfY3M6IlJ1bm5pbmci&pageSize=30&manufacturer=&resultCatEntryType=&catalogId=40000000629&pageView=image&searchTerm=&facet=ads_f104501_ntk_cs%253A%2522CL%2B42%2B%257C%2BUS%2B10%2522&minPrice=&categoryId=51206332&storeId=10801'
    response = urllib2.urlopen(url)
    html = response.read()



    #detalle
    detalle = re.findall(r'<div class="description_fixedwidth"(.+?)</div>', html, flags = re.DOTALL)
    _detalle_=[]
    for i in range(len(detalle)):
        detalle_ = re.findall(r'href(.+?)a>', detalle[i] , flags = re.DOTALL)
        detalle_ = re.findall(r'Zapatilla(.+?)\r', detalle_[0] , flags = re.DOTALL)
        _detalle_.append(detalle_)

    #marca
    marca=[]
    for i in _detalle_:
        marca.append(re.findall(r' [A-Za-z]* ', i[0], flags = re.DOTALL))

    #precio
    precio = re.findall(r'<div id="offerPrice" class="price offerPrice bold">(.+?)</div>', html ,flags = re.DOTALL)
    precio_=[]
    for i in precio:
        precio_.append(re.findall(r'\w+.\w+',i,flags = re.DOTALL))

    min_=precio_.index(min(precio_))

    datos=[_detalle_[min_][0],toint(precio_[min_][0])]
    return datos




def main():
    datos38 = talla38()
    datos40 = talla40()
    datos42 = talla42()
    totalmin=datos38[1]+datos40[1]+datos42[1]
    print "Paris        |","Modelo    |",datos38[0][0:12], "|  ", datos40[0][0:12], " |  ", datos42[0][0:12], " |    -"
    print "             |           |",datos38[0][13:26],  "|  ", datos40[0][13:26],  " |  ", datos42[0][13:26 ], "|    -"
    print "              ______________________________________________________________"
    print "            ","|Precio     |", datos38[1], "       |", datos40[1], "     |", datos42[1], "             |    ", totalmin
    return totalmin
if __name__ == "__main__":
    main()
