import urllib2  # descargar una url
import re  #  expresiones regulares


def toint(precio):
    digitos = re.findall(r'([0-9]+)', precio)
    entero = int(''.join(digitos))
    return entero


def talla38():
    url ='http://simple.ripley.cl/deporte/zapatillas/zapatillas-hombre?pageSize=24&orderBy=3&manufacturer=NIKE'
    # url = 'http://simple.ripley.cl/deporte/zapatillas/zapatillas-mujer?pageSize=24&orderBy=3&facet=ads_f714001_ntk_cs%253A%252238%2522'
    response = urllib2.urlopen(url)
    html = response.read()
    detalle = re.findall(r'<div class="product-description">(.+?)</div>', html, flags = re.DOTALL)
    precio = re.findall(r'<span class="catalog-product-offer-price best-price">(.+?)</span>', detalle[0])
    precio = re.findall(r'[0-9.]+', precio[0])
    detalle = re.findall(r'ZAPATILLA (.+)',re.findall(r'<div><span class="js-clamp catalog-product-name" data-clamp="3">(.+?)</span>', detalle[0])[0])
    precio = toint(precio[0])
    detalle = re.findall(r'[A-Z0-9]+',detalle[0])
    info1 = detalle[0]+" "+detalle[1]
    info2 = detalle[2]
    datos = [info1, info2, precio]
    return datos


def talla40():
    url = 'http://simple.ripley.cl/deporte/zapatillas/zapatillas-hombre?pageSize=24&orderBy=3&facet=ads_f712001_ntk_cs%253A%2522Running%2522'
    response = urllib2.urlopen(url)
    html = response.read()
    detalle = re.findall(r'<div class="product-description">(.+?)</div>', html, flags = re.DOTALL)
    precio = re.findall(r'<span class="catalog-product-offer-price best-price">(.+?)</span>', detalle[0])
    precio = re.findall(r'[0-9.]+', precio[0])
    detalle = re.findall(r'ZAPATILLA (.+)',re.findall(r'<div><span class="js-clamp catalog-product-name" data-clamp="3">(.+?)</span>', detalle[0])[0])
    precio = toint(precio[0])
    detalle = re.findall(r'[A-Z0-9]+',detalle[0])
    info1 = detalle[0]+" "+detalle[1]
    info2 = detalle[2]
    datos = [info1, info2, precio]
    return datos


def talla42():
    url = 'http://simple.ripley.cl/deporte/zapatillas/zapatillas-hombre?pageSize=24&orderBy=3&facet=ads_f714001_ntk_cs%253A%252242%2522'
    response = urllib2.urlopen(url)
    html = response.read()
    detalle = re.findall(r'<div class="product-description">(.+?)</div>', html, flags = re.DOTALL)
    precio = re.findall(r'<span class="catalog-product-offer-price best-price">(.+?)</span>', detalle[0])
    precio = re.findall(r'[0-9.]+', precio[0])
    detalle = re.findall(r'ZAPATILLA (.+)',re.findall(r'<div><span class="js-clamp catalog-product-name" data-clamp="3">(.+?)</span>', detalle[0])[0])
    precio = toint(precio[0])
    detalle = re.findall(r'[A-Z0-9]+',detalle[0])
    info1 = detalle[0]+" "+detalle[1]
    info2 = detalle[2]
    datos = [info1, info2, precio]
    return datos


def main():
    datos38 = talla38()
    datos40 = talla40()
    datos42 = talla42()
    totalmin=datos38[2]+datos40[2]+datos42[2]
    print "Ripley       |","Modelo    |",datos38[0], "     |  ", datos40[0], "|  ", datos42[0], " |  -"
    print "             |           |",datos38[1], "         |   ", datos40[1], "   |  ", datos42[1], " |"
    print "              ______________________________________________________________"
    print "            ","|Precio     |", datos38[2], "       |", datos40[2], "     |", datos42[2], "              |", datos38[2]+datos40[2]+datos42[2]
    return totalmin
if __name__ == "__main__":
    main()
