import falabella
import ripley
import paris

def main():
    print "____________________________________________________________________________________________"
    print "Tienda       |           |Talla 38         |Talla 40       |Talla 42       |  Total Compra"
    print "____________________________________________________________________________________________"
    a=falabella.main()
    print "____________________________________________________________________________________________"
    b=ripley.main()
    print "____________________________________________________________________________________________"
    c=paris.main()
    print "____________________________________________________________________________________________"
    print '\nLe combiene mas comprar en:'
    if min(a,b,c)==a:
        print 'Falabella: \nTotal Compra',a
    elif min(a,b,c)==b:
        print 'Ripley: \nTotal Compra',b
    else :
        print 'Paris: \nTotal Compra',c
if __name__ == "__main__":
    main()
