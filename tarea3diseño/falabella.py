import urllib2  # descargar una url
import re  #  expresiones regulares


def toint(precio):
    digitos = re.findall(r'([0-9]+)', precio)
    entero = int(''.join(digitos))
    return entero


def talla38():
    url = 'http://www.falabella.com/falabella-cl/category/cat850064/Zapatillas-Hombre/N-1z1178mZ1z141u8?sorter=1'
    response = urllib2.urlopen(url)
    html = response.read()
    detalle = re.findall(r'<div class="detalle">(.+?)</div>', html, flags = re.DOTALL)
    marca = re.findall(r'<div class="marca">(.+?)</div>', html, flags = re.DOTALL)
    precio = re.findall(r'<div class="wishlistPrice1">(.+?)</div>', html, flags = re.DOTALL)
    detalle = re.findall(r'title="Zapatilla Running [A-Za-z]*(.+?)"',re.findall(r'<div class="detalle">(.+?)</div>', html, flags = re.DOTALL)[0], flags = re.DOTALL)[0]
    marca = re.findall(r'title="(.+?)"',marca[0], flags = re.DOTALL)[0]
    precio = re.findall(r'"unitPriceD">(.+?)<',precio[0], flags = re.DOTALL)[0]
    precio = toint(precio)
    datos = [marca, detalle, precio]
    return datos


def talla40():
    url = 'http://www.falabella.com/falabella-cl/category/cat850064/Zapatillas-Hombre/N-1z1178mZ1z141u5?sorter=1'
    response = urllib2.urlopen(url)
    html = response.read()
    detalle = re.findall(r'<div class="detalle">(.+?)</div>', html, flags = re.DOTALL)
    marca = re.findall(r'<div class="marca">(.+?)</div>', html, flags = re.DOTALL)
    precio = re.findall(r'<div class="wishlistPrice1">(.+?)</div>', html, flags = re.DOTALL)
    detalle = re.findall(r'title="Zapatilla Running [A-Za-z]*(.+?)"',detalle[0], flags = re.DOTALL)[0]
    marca = re.findall(r'title="(.+?)"',marca[0], flags = re.DOTALL)[0]
    precio = re.findall(r'"unitPriceD">(.+?)<',precio[0], flags = re.DOTALL)[0]
    precio = toint(precio)
    datos = [marca, detalle, precio]
    return datos


def talla42():
    url = 'http://www.falabella.com/falabella-cl/category/cat850064/Zapatillas-Hombre/N-1z1178mZ1z141u2?sorter=1'
    response = urllib2.urlopen(url)
    html = response.read()
    detalle = re.findall(r'<div class="detalle">(.+?)</div>', html, flags = re.DOTALL)
    marca = re.findall(r'<div class="marca">(.+?)</div>', html, flags = re.DOTALL)
    precio = re.findall(r'<div class="wishlistPrice1">(.+?)</div>', html, flags = re.DOTALL)
    detalle = re.findall(r'title="Zapatilla Running [A-Za-z]*(.+?)"',detalle[0], flags = re.DOTALL)[0]
    marca = re.findall(r'title="(.+?)"',marca[0], flags = re.DOTALL)[0]
    precio = re.findall(r'"unitPriceD">(.+?)<',precio[0], flags = re.DOTALL)[0]
    precio = toint(precio)
    datos = [marca, detalle, precio]
    return datos


def main():
    datos38 = talla38()
    datos40 = talla40()
    datos42 = talla42()
    totalmin=datos38[2]+datos40[2]+datos42[2]
    print "Falabella    |","Modelo    |",datos38[0], "   |  ", datos40[0], "   |  ", datos42[0], "   |       -"
    print "             |           |",datos38[1], "   |  ", datos40[1], "    |  ", datos42[1], "    |       -"
    print "              _______________________________________________________________________________"
    print "            ","|Precio     |", datos38[2], "          |", datos40[2], "       |", datos42[2], "         |", datos38[2]+datos40[2]+datos42[2]
    return totalmin
if __name__ == "__main__":
    main()
