import random
from time import time


def knapsack(items, maxweight):
    matrix = [[0] * (maxweight + 1)
                  for i in xrange(len(items) + 1)]
    # for i, (value, weight) in enumerate(items):
    for i, ( value,weight) in enumerate(items):
        i += 1
        for capacity in xrange(maxweight + 1):
            if weight > capacity:
                matrix[i][capacity] = matrix[i - 1][capacity]
            else:
                candidate1 = matrix[i - 1][capacity]
                candidate2 = matrix[i - 1][capacity - weight] + value
                matrix[i][capacity] = max(candidate1, candidate2)
    # Reconstruction
    reconstruction = []
    i = len(items)
    j = maxweight
    while i > 0:
        if matrix[i][j] != matrix[i - 1][j]:
            reconstruction.append(items[i - 1])
            j -= items[i - 1][1]
        i -= 1
    reconstruction.reverse()
    return matrix[len(items)][maxweight], reconstruction

# *** ejemplo ***

# items=[ [4,1],
#         [5,4],
#         [1,3],
#         [4,11],
#         [4,3] ]
# print knapsack(items,10)
# print items


#item[a,b] : primer valor peso,segundo precio
items=[]
times=[]
numbers_of_items=10000
maxweight=300

for x in range(1,1000 ,100):
    for i in range(x):
        weight=random.randrange(maxweight/2)+1
        price=random.randrange(maxweight/2)+1
        item=[weight,price]
        items.append(item)

    tini=time()
    knapsack(items,maxweight)
    tfinal=time()

    tiempo=tfinal-tini
    times.append(tiempo)
print times
